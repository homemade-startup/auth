FROM python:3.8

ARG APP_USER=appuser
RUN groupadd -r ${APP_USER} && useradd --no-log-init -r -g ${APP_USER} ${APP_USER}

RUN apt-get update && apt-get install -y cmake bison flex gettext

ENV PYTHONPATH=/usr/src/app/
ENV PATH="$PATH:/root/.local/bin"

WORKDIR /usr/local/auth-service/

ENV DJANGO_ENV=production \
    POETRY_VERSION=1.1.6

RUN apt-get update && apt-get install --no-install-recommends -y \
    curl \
    git \
    libpq-dev \
    wget \
    nginx \
    supervisor \
    cmake \
    bison \
    flex \
    gettext

RUN pip install --upgrade pip
RUN pip install "poetry==$POETRY_VERSION" && poetry --version

COPY poetry.lock poetry.lock
COPY pyproject.toml pyproject.toml

RUN poetry config virtualenvs.create false && poetry install --no-interaction --no-ansi

COPY . .

EXPOSE 9090

COPY entrypoint.sh /usr/local/bin/
RUN chmod 777 /usr/local/bin/entrypoint.sh
RUN ln -s /usr/local/bin/entrypoint.sh /

ENTRYPOINT ["entrypoint.sh"]
