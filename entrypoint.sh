#!/bin/bash

alembic upgrade head && gunicorn auth.app:wsgi --bind 0.0.0.0:9090 -w 4 --worker-class aiohttp.GunicornWebWorker --log-level=debug --access-logfile '-'
