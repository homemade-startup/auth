from tartiflette import TartifletteError


class DomainException(TartifletteError):
    msg: str
    usr_msg: str

    def __init__(self, *args, **kwargs):
        super().__init__(message=self.msg, *args, **kwargs)
        self.user_message = self.usr_msg


class WrongCodeException(DomainException):
    msg: str = 'Wrong code.'
    usr_msg: str = 'Неверный код подтверждения.'


class ThrottleException(DomainException):
    msg: str = 'Try again later.'
    usr_msg: str = 'Повторите позже.'


class InvalidPhoneException(DomainException):
    msg: str = 'Invalid phone number.'
    usr_msg: str = 'Не верный формат номера телефона.'
