from sqlalchemy import (
    Column, Integer, String, DateTime, Enum, UniqueConstraint, column
)
from sqlalchemy import func

from auth.database import Base
from auth.entities.verify import AccountType


class User(Base):

    __tablename__ = 'users'
    
    __table_args__ = (
        UniqueConstraint(column('phone'), column('account_type')),
    )

    id = Column(Integer, primary_key=True)
    phone = Column(String(32), nullable=False)
    account_type = Column(Enum(AccountType), nullable=False)
    created_at = Column(DateTime, default=func.now(), nullable=False)
