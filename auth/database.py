import aioredis

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import sessionmaker


async def create_async_session(engine):
    async_session = sessionmaker(
        engine, expire_on_commit=False, class_=AsyncSession
    )
    async with async_session() as session:
        yield session


async def create_redis(uri: str):
    redis = await aioredis.create_redis_pool(uri)
    yield redis
    redis.close()
    await redis.wait_closed()


Base = declarative_base()
