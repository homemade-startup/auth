from datetime import datetime
from enum import Enum

import phonenumbers
import attr
from phonenumbers.phonenumberutil import NumberParseException

from auth.exceptions import InvalidPhoneException


class AccountType(Enum):
    buyer = 'BUYER'
    seller = 'SELLER'


def parse_phone(phone: str) -> str:
    try:
        phonenumbers.format_number(
            phonenumbers.parse(phone),
            phonenumbers.PhoneNumberFormat.E164
        )
    except NumberParseException as error:
        raise InvalidPhoneException() from error


@attr.s(
    auto_attribs=True,
    slots=True,
)
class SendCode:
    phone: str = attr.ib(converter=parse_phone)


@attr.s(
    auto_attribs=True,
    slots=True,
)
class SendCodeResult:
    uuid: str = attr.ib()
    expire: datetime = attr.ib()


@attr.s(
    auto_attribs=True,
    slots=True,
)
class Signin:
    phone: str = attr.ib(converter=parse_phone)
    code: str = attr.ib()
    uuid: str = attr.ib()
    account_type: AccountType = attr.ib()


@attr.s(
    auto_attribs=True,
    slots=True,
)
class Token:
    value: str = attr.ib()
