"""Main container."""

from dependency_injector import containers, providers
from sqlalchemy.ext.asyncio import create_async_engine
from twilio.rest import Client

from auth.config import settings as config
from auth.database import create_async_session, create_redis
from auth.services.sms_sender import ISmsSenderService
from auth.services.verify import IVerifyService
from auth.services import (
    TwilioSmsSenderService,
    VerifyService,
    StorageService,
    AuthService,
    FakeVerifyService, 
    FakeSmsSenderService,
    ThrottleService,
)


class Container(containers.DeclarativeContainer):

    settings = providers.Singleton(config)

    engine = providers.Singleton(
        create_async_engine,
        config.DATABASE_URI,
    )

    session = providers.Resource(
        create_async_session,
        engine=engine,
    )

    redis = providers.Resource(
        create_redis,
        config.REDIS_URI,
    )

    twilio = providers.Singleton(
        Client,
        username=config.TWILIO_SID,
        password=config.TWILIO_TOKEN,
    )

    sms_sender: ISmsSenderService = providers.Factory(
        TwilioSmsSenderService,
        twilio=twilio,
    )
    
    fake_sms_sender: ISmsSenderService = providers.Factory(
        FakeSmsSenderService,
    )

    storage = providers.Factory(
        StorageService,
        redis=redis,
    )
    
    throttle_service = providers.Factory(
        ThrottleService,
        redis=redis,
        limit=config.SMS_THROTTLING_LIMIT,
        timeout=config.SMS_THROTTLING_TIMEOUT,
    )

    verify_service: IVerifyService = providers.Factory(
        # VerifyService,
        FakeVerifyService,
        storage=storage,
        # sms_sender=sms_sender,
        sms_sender=fake_sms_sender,
        throttle_service=throttle_service,
    )

    fake_verify_service = providers.Factory(
        FakeVerifyService,
        storage=storage,
        sms_sender=fake_sms_sender,
        throttle_service=throttle_service,
    )

    auth_service = providers.Factory(
        AuthService,
        # verify_service=verify_service,
        verify_service=fake_verify_service,
        session=session,
    )
