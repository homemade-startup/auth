import os
import logging

from aiohttp import web
from tartiflette_aiohttp import register_graphql_handlers

from auth.config import settings
from auth.containers import Container
from auth.views import HealthView


def app_factory():
    from auth import resolvers, views
    container = Container()
    container.wire(packages=[resolvers, views])

    app = web.Application(debug=True)

    app['settings'] = settings
    
    logging.basicConfig(level=logging.DEBUG)
    
    app.router.add_view("/health", HealthView)

    graphql_handlers = register_graphql_handlers(
        app=app,
        engine_sdl=[
            os.path.join('', "auth/sdl/Query.graphql"),
            os.path.join('', "auth/sdl/Mutation.graphql"),
            os.path.join('', "auth/sdl/Subscription.graphql"),
        ],
        engine_modules=[
            "auth.resolvers.mutation",
        ],
        executor_http_endpoint="/graphql",
        executor_http_methods=["POST"],
        graphiql_enabled=True,
        subscription_ws_endpoint="/ws",
    )

    return graphql_handlers


async def wsgi():
    return app_factory()


def run() -> None:
    """
    Entry point of the application.
    """
    web.run_app(app_factory())
