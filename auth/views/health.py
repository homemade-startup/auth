from aiohttp import web
from dependency_injector.wiring import inject, Provide
from sqlalchemy import text
from sqlalchemy.orm import Session
from aioredis import Redis

from auth.containers import Container


class HealthView(web.View):
    @inject
    async def get(
        self,
        session: Session = Provide[Container.session],
        redis: Redis = Provide[Container.redis],
    ):
        result = await session.execute(text('select 1;'))
        _ = result.scalars().one()
        _ = await redis.get('health')
        return web.Response(text='Status: SUCCESS')
