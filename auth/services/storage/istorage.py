from auth.iservice import IService


class IStorageService(IService):
    async def save(self, phone: str, uuid: str, code: str) -> None:
        raise NotImplementedError()
    
    async def clean(self, phone: str, uuid: str) -> None:
        raise NotImplementedError()

    async def check(self, phone: str, uuid: str, code: str) -> bool:
        raise NotImplementedError()
