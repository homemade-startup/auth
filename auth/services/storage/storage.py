from typing import Optional
from datetime import datetime, timedelta
from aioredis import Redis

from auth.config import settings
from auth.services.storage import IStorageService


class StorageService(IStorageService):
    def __init__(self, redis: Redis) -> None:
        super().__init__()
        self.redis = redis

    async def save(self, phone: str, uuid: str, code: str) -> None:
        await self.redis.set(
            self.generate_key(phone, uuid),
            self.generate_value(code),
        )
        await self.redis.expire(
            self.generate_key(phone, uuid),
            timeout=settings.SEND_CODE_TIMEOUT_IN_SECONDS,
        )

    async def check(self, phone: str, uuid: str, code: str) -> bool:
        correct_code = await self.redis.get(
            self.generate_key(phone, uuid),
            encoding="utf-8",
        )
        return correct_code is not None and correct_code == code

    async def clean(self, phone: str, uuid: str) -> None:
        _ = await self.redis.delete(self.generate_key(phone, uuid))

    async def get_expire(self, phone: str, uuid: str) -> Optional[datetime]:
        ttl = await self.redis.ttl(self.generate_key(phone, uuid))
        return None if ttl <= 0 else datetime.utcnow() + timedelta(seconds=ttl)

    @classmethod
    def generate_key(cls, phone: str, uuid: str) -> str:
        return f'avk:{phone}:{uuid}'

    @classmethod
    def generate_value(cls, code: str) -> str:
        return code
