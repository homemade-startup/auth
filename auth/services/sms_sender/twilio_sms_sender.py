import asyncio

from twilio.rest import Client
from twilio.rest.api.v2010.account.message import MessageInstance

from auth.config import settings
from auth.entities.verify import SendCode
from auth.services.sms_sender import ISmsSenderService


class TwilioSmsSenderService(ISmsSenderService):
    def __init__(self, twilio: Client) -> None:
        super().__init__()
        self.twilio = twilio

    async def send(self, send_code: SendCode, code: str):
        message = await asyncio.get_event_loop().run_in_executor(
            None,
            self.sync_send,
            send_code,
            code,
        )
        self.logger.info(
            'Sended message to %s. Status: %s. Price: %s',
            send_code.phone,
            message.status,
            message.price,
        )

    def sync_send(self, send_code: SendCode, code: str) -> MessageInstance:
        return self.twilio.messages.create(
            body=self.template.format(code=code),
            from_=settings.TWILIO_PHONE,
            to=send_code.phone,
        )
