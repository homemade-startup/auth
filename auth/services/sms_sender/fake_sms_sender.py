from auth.entities.verify import SendCode
from auth.services.sms_sender.isms_sender import ISmsSenderService


class FakeSmsSenderService(ISmsSenderService):
    def __init__(self) -> None:
        super().__init__()

    async def send(self, send_code: SendCode, code: str):
        self.logger.info('Sended message to %s.', send_code.phone)
