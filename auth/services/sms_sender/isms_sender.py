from auth.iservice import IService

from auth.entities.verify import SendCode


class ISmsSenderService(IService):
    template = 'Код подтверждения: {code}'

    async def send(self, send_code: SendCode, code: str):
        raise NotImplementedError()
