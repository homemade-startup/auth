from auth.iservice import IService
from auth.entities.verify import SendCode, SendCodeResult, Signin


class IVerifyService(IService):
    async def send_code(self, send_code: SendCode) -> SendCodeResult:
        raise NotImplementedError()

    async def verify(self, signin: Signin) -> bool:
        raise NotImplementedError()
    
    async def clean(self, signin: Signin) -> None:
        raise NotImplementedError()
