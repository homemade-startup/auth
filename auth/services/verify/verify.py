import string
import secrets
from uuid import uuid4
from datetime import datetime, timedelta

from auth.config import settings
from auth.exceptions import ThrottleException
from auth.iservice import IService
from auth.entities.verify import SendCode, SendCodeResult, Signin
from auth.services.sms_sender import ISmsSenderService
from auth.services.storage import StorageService
from auth.services.verify import IVerifyService
from auth.services.throttle import ThrottleService


class VerifyService(IVerifyService):

    def __init__(
        self,
        storage: StorageService,
        sms_sender: ISmsSenderService,
        throttle_service: ThrottleService,
    ) -> None:
        super().__init__()
        self.storage = storage
        self.sms_sender = sms_sender
        self.throttle_service = throttle_service

    async def send_code(self, send_code: SendCode) -> SendCodeResult:
        code = self.generate_code(send_code)
        uuid = str(uuid4())

        is_available = await self.throttle_service.is_available(send_code.phone)
        if not is_available:
            raise ThrottleException()
        await self.throttle_service.use(send_code.phone)

        await self.storage.save(phone=send_code.phone, uuid=uuid, code=code)

        await self.sms_sender.send(send_code=send_code, code=code)

        self.logger.debug('Send code. Phone: %s, code: %s', send_code.phone, code)

        return SendCodeResult(
            uuid=uuid,
            expire=(
                datetime.utcnow() +
                timedelta(seconds=settings.SEND_CODE_TIMEOUT_IN_SECONDS)
            )
        )

    async def verify(self, signin: Signin) -> bool:
        return await self.storage.check(
            phone=signin.phone,
            uuid=signin.uuid,
            code=signin.code,
        )
        
    async def clean(self, signin: Signin) -> None:
        await self.storage.clean(phone=signin.phone, uuid=signin.uuid)

    def generate_code(self, send_code: SendCode, alphabet=string.digits, length=4) -> str:
        return ''.join(secrets.choice(alphabet) for _ in range(length))
