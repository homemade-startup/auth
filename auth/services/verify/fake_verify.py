import string

from auth.entities.verify import SendCode
from auth.services.verify import VerifyService


class FakeVerifyService(VerifyService):
    def generate_code(self, send_code: SendCode, alphabet=string.digits, length=4) -> str:
        return send_code.phone[-4:]
