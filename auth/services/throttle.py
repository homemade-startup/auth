from datetime import time
from aioredis import Redis

from auth.iservice import IService


class ThrottleService(IService):
    def __init__(self, redis: Redis, limit: int, timeout: int) -> None:
        super().__init__()
        self.redis = redis
        self.limit = limit
        self.timeout = timeout

    async def use(self, key: str) -> None:
        value = int(await self.redis.incr(key))
        if value > 1:
            await self.redis.expire(key, timeout=self.timeout)
    
    async def is_available(self, key: str) -> bool:
        count = await self.redis.get(key, encoding='utf-8')
        return count is None or int(count) < self.limit
