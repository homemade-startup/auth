from datetime import datetime

import jwt
from sqlalchemy import select, and_
from sqlalchemy.orm import Session

from auth.config import settings
from auth.iservice import IService
from auth.exceptions import WrongCodeException
from auth.db import User
from auth.entities.verify import Signin, Token
from auth.services.verify import VerifyService


class AuthService(IService):

    def __init__(self, verify_service: VerifyService, session: Session) -> None:
        super().__init__()
        self.verify_service = verify_service
        self.session = session

    async def signin(self, signin: Signin) -> Token:
        is_success = await self.verify_service.verify(signin)
        if not is_success:
            raise WrongCodeException()

        user = await self.get_or_create_user(signin)

        return Token(value=self.generate_token(user))

    async def get_or_create_user(self, signin: Signin) -> User:
        result = await self.session.execute(select(User).where(and_(
            User.phone == signin.phone,
            User.account_type == signin.account_type,
        )))
        user = result.scalars().one_or_none()

        if not user:
            user = User(
                phone=signin.phone,
                account_type=signin.account_type,
                created_at=datetime.utcnow()
            )
            self.session.add(user)
            await self.session.commit()

        return user

    def generate_token(self, user: User) -> str:
        payload = {
            "https://hasura.io/jwt/claims": {
                "x-hasura-allowed-roles": [user.account_type.value],
                "x-hasura-default-role": user.account_type.value,
                "x-hasura-user-id": str(user.id),
                "x-hasura-user-phone": user.phone,
            }
        }
        token = jwt.encode(payload, settings.HASURA_JWT_SECRET, settings.HASURA_JWT_ALGORITHM)
        return token.decode("utf-8")

    def get_anonymous_token(self) -> Token:
        payload = {
            "https://hasura.io/jwt/claims": {
                "x-hasura-allowed-roles": ['ANONYMOUS'],
                "x-hasura-default-role": 'ANONYMOUS',
                "x-hasura-user-id": '0',
            }
        }
        token = jwt.encode(payload, settings.HASURA_JWT_SECRET, settings.HASURA_JWT_ALGORITHM)
        return Token(value=token.decode("utf-8"))
