import humps

import cattr

class CAttrConverter:

    converter = cattr.Converter()

    def __init__(self):
        """
        structure hook for load
        unstructure hook for dump
        """

    @classmethod
    def load(cls, params, data_cls, camel_to_snake=True):
        """
        :param params: params, mostly from front end
        :param data_cls:
        :param camel_to_snake: need to convert from camel style to snake style
        """
        if camel_to_snake:
            params = humps.depascalize(params)
        return cls.converter.structure(params, data_cls)

    @classmethod
    def dump(cls, data, snake_to_camel=True):
        """
        :param data:
        :param snake_to_camel: dump as camel case
        """
        result: dict = cls.converter.unstructure(data)
        if snake_to_camel:
            result = humps.camelize(result)

        return result
