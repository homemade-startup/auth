#!/usr/bin/env python
import sys

from auth.app import run

if __name__ == "__main__":
    sys.exit(run())
