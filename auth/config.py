from dynaconf import Dynaconf

settings = Dynaconf(
    envvar_prefix="AUTH",
    settings_files=["config/settings.yaml", "config/.secrets.yaml"],
    environments=True,
    load_dotenv=True,
    env_switcher="AUTH_ENV",
)
