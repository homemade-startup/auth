from typing import Any, Dict, Optional

import attr
from tartiflette import Resolver
from dependency_injector.wiring import inject, Provide

from auth.entities import SendCode, Signin
from auth.containers import Container
from auth.attrs import CAttrConverter
from auth.services import VerifyService, AuthService


@Resolver("Mutation.sendCode")
@inject
async def resolve_mutation_send_code(
    parent: Optional[Any],
    args: Dict[str, Any],
    ctx: Dict[str, Any],
    info: "ResolveInfo",
    verify_service: VerifyService = Provide[Container.verify_service],
) -> Dict[str, Any]:
    """
    Resolver in charge of the mutation of a recipe.
    :param parent: initial value filled in to the engine `execute` method
    :param args: computed arguments related to the mutation
    :param ctx: context filled in at engine initialization
    :param info: information related to the execution and field resolution
    :type parent: Optional[Any]
    :type args: Dict[str, Any]
    :type ctx: Dict[str, Any]
    :type info: ResolveInfo
    :return: the mutated recipe
    :rtype: Dict[str, Any]
    :raises Exception: if the recipe id doesn't exist
    """
    send_code = CAttrConverter.load(args["input"], SendCode)
    result = await verify_service.send_code(send_code)
    return CAttrConverter.dump(attr.asdict(result))


@Resolver("Mutation.signin")
@inject
async def resolve_mutation_signin(
    parent: Optional[Any],
    args: Dict[str, Any],
    ctx: Dict[str, Any],
    info: "ResolveInfo",
    auth_service: AuthService = Provide[Container.auth_service],
) -> Dict[str, Any]:
    """
    Resolver in charge of the mutation of a recipe.
    :param parent: initial value filled in to the engine `execute` method
    :param args: computed arguments related to the mutation
    :param ctx: context filled in at engine initialization
    :param info: information related to the execution and field resolution
    :type parent: Optional[Any]
    :type args: Dict[str, Any]
    :type ctx: Dict[str, Any]
    :type info: ResolveInfo
    :return: the mutated recipe
    :rtype: Dict[str, Any]
    :raises Exception: if the recipe id doesn't exist
    """
    signin_input = CAttrConverter.load(args["input"], Signin)
    result = await auth_service.signin(signin_input)
    return CAttrConverter.dump(attr.asdict(result))


@Resolver("Mutation.getAnonymousToken")
@inject
async def resolve_mutation_get_anonymous_token(
    parent: Optional[Any],
    args: Dict[str, Any],
    ctx: Dict[str, Any],
    info: "ResolveInfo",
    auth_service: AuthService = Provide[Container.auth_service],
) -> Dict[str, Any]:
    """
    Resolver in charge of the mutation of a recipe.
    :param parent: initial value filled in to the engine `execute` method
    :param args: computed arguments related to the mutation
    :param ctx: context filled in at engine initialization
    :param info: information related to the execution and field resolution
    :type parent: Optional[Any]
    :type args: Dict[str, Any]
    :type ctx: Dict[str, Any]
    :type info: ResolveInfo
    :return: the mutated recipe
    :rtype: Dict[str, Any]
    :raises Exception: if the recipe id doesn't exist
    """
    result = auth_service.get_anonymous_token()
    return CAttrConverter.dump(attr.asdict(result))
