"""Init

Revision ID: 252fc3a534b0
Revises: 
Create Date: 2021-05-18 19:58:07.476757

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '252fc3a534b0'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('users',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('phone', sa.String(length=32), nullable=False),
    sa.Column('account_type', sa.Enum('buyer', 'seller', name='accounttype'), nullable=False),
    sa.Column('created_at', sa.DateTime(), nullable=False),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('phone', 'account_type')
    )


def downgrade():
    op.drop_table('users')
    op.execute('DROP TYPE accounttype;')
